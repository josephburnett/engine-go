module gitlab.com/josephburnett/engine-go

go 1.21.0

require gitlab.com/josephburnett/step-runner v0.0.0-20230923001843-41e9a6b9ce5d

require google.golang.org/protobuf v1.31.0 // indirect
