FROM golang:1.21

WORKDIR /engine

COPY go.mod go.sum ./
RUN go mod download

COPY *.go ./

RUN CGO_ENABLED=0 GOOS=linux go build -o /engine-go

CMD ["/engine-go"]
