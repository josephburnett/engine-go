package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
)

var (
	target = flag.String("target", "", "Where is the golang step to execute?")
)

func main() {
	flag.Parse()
	if err := run(); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	os.Exit(0)
}

func run() error {
	if *target == "" {
		return fmt.Errorf("--target is required to point to the golang step to execute")
	}
	cmd := exec.Command("go", "run", ".")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Dir = *target
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("running go: %v", err)
	}
	return nil
}
