.PHONY : build
build :
	docker build -t registry.gitlab.com/josephburnett/engine-go .

.PHONY : push
push :
	docker push registry.gitlab.com/josephburnett/engine-go
